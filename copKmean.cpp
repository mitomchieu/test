#include <bits/stdc++.h>
#define maxN 2000

using namespace std;
struct point {
	double a[maxN];
	int len = 0;
	void xuat() {
		for (int i = 0; i < len; i++) cout << a[i] << ' ' ;
		cout << endl;
	}
};

string name = "data/glass";
point A[maxN], cen[maxN]; //data, centroid
vector <int> link[maxN], notLink[maxN];
int L[maxN], preL[maxN], ansL[maxN], Label_ans[maxN];
long long n, m = 0, k;

void read(int i) {
	string s;
	getline(cin, s);
	istringstream ss(s);
	double x;
	while (ss >> x) {
		A[i].a[A[i].len++] = x;
	}
}

double operator - (point x, point y) {
	double s = 0;
	for (int i = 0; i < x.len; i++) 
		s += (x.a[i] - y.a[i])*(x.a[i] - y.a[i]);
	return sqrt(s);
}

point operator + (point x, point y) {
	for (int i = 0; i < x.len; i++) 
		x.a[i] += y.a[i];
	return x;
}

point operator * (point x, int k) {
	for (int i = 0; i < x.len; i++) 
		x.a[i] *= k;
	return x;
}

point operator / (point x, int k) {
	for (int i = 0; i < x.len; i++) 
		x.a[i] /= k;
	return x;
}

void Input () {
	freopen(&(name + ".inp")[0], "r", stdin);
	cin >> n >> k;
	string s;
	getline(cin, s);
	for (int i = 0; i < n; i++) read(i);
	fclose(stdin);
	freopen(&(name + "_constraints.inp")[0], "r", stdin);
	int u, v, K;
	while (cin >> u >> v >> K) {
		m++;
		u--; v--;
		if (K == 1) {
			link[u].push_back(v);
			link[v].push_back(u);
		}
		else {
			notLink[u].push_back(v);
			notLink[v].push_back(u);
		}
	}
	fclose(stdin);
	cout << "Number of constraints :" << m << endl;
}

void Init() {
	for (int i = 0; i < n; i++) L[i] = -1;
	int c[maxN] = {0};
	for (int i = 0; i < k; i++) {
		do {
			int pos = rand() % n;
			for (int i = 0; i < link[pos].size(); i++) {
				int x = link[pos][i];
				if (c[x] == 1) continue;
			}
			c[pos] = 1;
			//cout << pos << ' ';
			cen[i] = A[pos];
			//L[pos] = i;
			break;
		} while (1);
	}
	//cout << endl;
}

void findCen() {
	for (int i = 0; i < n; i++) L[i] = -1;
	for (int i = 0; i < n; i++) 
		if (L[i] == -1) {
			for (int j = 0; j < link[i].size(); j++) {
				int u = link[i][j];
				if (L[u] >= 0) {
					L[i] = L[u];
					break;
				}
			}
			if (L[i] >= 0) continue;
			set <int> s;
			for (int j = 0; j < k; j++) s.insert(j);
			for (int j = 0; j < notLink[i].size(); j++) {
				int u = notLink[i][j];
				if (L[u] >= 0) {
					if (s.count(L[u])) s.erase(L[u]);
				}
			}
			double Min = 1000000000;
			int pos = -1;
			for (int j = 0; j < k; j++) 
				if (s.count(j)) {
					if (A[i] - cen[j] < Min) {
						Min = A[i] - cen[j];
						pos = j;
					}
				}
			if (pos != -1) L[i] = pos;
		}
}

void setCen() {
	int F[maxN] = {0};
	for (int i = 0; i < n; i++) {
		//cout << L[i] << endl;
		if (F[L[i]] == 0) {
			cen[L[i]] = A[i];
		}
		else cen[L[i]] = cen[L[i]] + A[i];
		F[L[i]]++;
	}
	for (int i = 0; i < k; i++){
		cen[i] = cen[i] / F[i];
	} 
	//cout << "////////////////////////"<< endl;
}

void storeLabel() {
	for (int i = 0; i < n; i++) 
		preL[i] = L[i];
}

int change() {
	for (int i = 0; i < n; i++)
		if (preL[i] != L[i]) return 1;
	return 0;
}

double calRandIndex() {
	ifstream fin(&(name + "_goc.inp")[0]);
	for (int i = 0; i < n; i++) {
		fin >> ansL[i];
	}
	fin.close();
	double ans = 0;
	int cnt = 0;
	for (int i = 0; i < n; i++)
		for (int j = i+1; j < n; j++) {
			if (L[i] == L[j] && ansL[i] == ansL[j]) cnt++;
			if (L[i] != L[j] && ansL[i] != ansL[j]) cnt++;
		}
	return (double)cnt * 2/ (n * (n-1));
}

void writeOut() {
	freopen(&(name + ".out")[0], "w" , stdout);
	for (int i = 0; i < n; i++) {
		cout << Label_ans[i] << endl;
	}
	fclose(stdout);
}

void Solve () {
	double ans = 0;
	for (int i = 0; i < 30; i++) {
		Init();
		do {
			findCen();
			setCen();
			if (!change()) break;
			storeLabel();
		} while(1);
		double tmp = calRandIndex();
		if (tmp > ans) {
			ans = tmp;
			for (int j = 0; j < n; j++) Label_ans[j] = L[j];
		}
	}
	cout << "Max Rand Index:" << ans << endl;
	writeOut();
}

main () {
	 srand(time(0)*time(0));
	 Input();
	 Solve();
}
